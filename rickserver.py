import http.server as h
import socketserver as s

class M(h.BaseHTTPRequestHandler):

    def do_GET(x):
        x.send_response(302)
        x.send_header('Location', "http://youtu.be/dQw4w9WgXcQ")
        x.end_headers()


s.TCPServer(('', 80), M).serve_forever()
