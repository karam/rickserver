### The most basic web server

This is the smallest web server I can write that is able to do something
useful... Only eight lines of code!

#### Usage

```sh
sudo python3 rickserver.py &
```

And then, in your web browser, navigate to `http://localhost/`. Good luck :)
